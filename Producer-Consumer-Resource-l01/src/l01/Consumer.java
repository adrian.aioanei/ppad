package l01;

public class Consumer extends Thread {
	Resource _res;
	private int _idConsumer;

	Consumer(Resource res, int idConsumer) {
		this._res = res;
		this._idConsumer = idConsumer;
	}

	@Override
	public void run() {
		for (int i = 0; i < Main.numberOfIterations; i++) {
			String s = _res.getResource();
			System.out.println("Consumer " + _idConsumer + " received value : " + s);
		}
	}
}

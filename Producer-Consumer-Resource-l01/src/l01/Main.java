package l01;

public class Main {

	public static final int numberOfIterations = 10;

	public static void main(String args[]) {
		try {
			Resource res = new Resource();
			Consumer consumer = new Consumer(res, 1);
			Producer producer = new Producer(res, 1);

			consumer.start();
			producer.start();

			consumer.join();
			producer.join();

			System.out.println("In vector have left : " + res.getVectorSize() + " elements.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}

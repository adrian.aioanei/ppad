package l01;

public class Producer extends Thread {
	private Resource _res;
	private int _idProducer;

	Producer(Resource res, int idProducer) {
		this._res = res;
		this._idProducer = idProducer;
	}

	@Override
	public void run() {
		for (int i = 0; i < Main.numberOfIterations; i++) {
			String s = "Test" + i;
			_res.putResource(s);
			System.out.println("Producer  " + _idProducer + " has put value " + s);
		}
	}
}

package l01;

import java.util.Vector;

public class Resource {

	private Vector<String> _resource;
	private volatile boolean flag;

	Resource() {
		_resource = new Vector<String>();
		flag = false;
	}

	public synchronized String getResource() {
		while (false == flag) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("Error occurred");
			}
		}
		flag = false;
		notifyAll();
		return _resource.isEmpty() ? "Empty Vector" : _resource.remove(_resource.size() - 1);
	}

	public synchronized void putResource(String inputString) {
		while (true == flag) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("Error occurred");
			}
		}
		flag = true;
		_resource.addElement(inputString);
		notifyAll();
	}

	public int getVectorSize() {
		return _resource.size();
	}
}
